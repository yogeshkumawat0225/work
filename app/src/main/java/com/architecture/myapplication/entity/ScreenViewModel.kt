package com.architecture.myapplication.entity

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class ScreenViewModel : ViewModel() {

    val buttonText = MutableLiveData<String>().apply { value = "Fetch" }

    val textViewMessage = MutableLiveData<String>().apply { value = "Success" }

    val buttonVisibility = MutableLiveData<Boolean>().apply { value = true }

    val textVisibility = MutableLiveData<Boolean>().apply { value = false }

    val progressBarVisibility = MutableLiveData<Boolean>().apply { value = false }

}