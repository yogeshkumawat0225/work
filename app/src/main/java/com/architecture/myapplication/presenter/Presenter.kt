package com.architecture.myapplication.presenter

import com.architecture.myapplication.entity.ScreenViewModel
import com.architecture.myapplication.view.OnDataFetched

//Presenter
class Presenter: OnDataFetched {

    var modelScreen: ScreenViewModel? = null

    override fun onDataFetchSuccess(response: String) {
        modelScreen?.buttonVisibility?.value = true
        modelScreen?.progressBarVisibility?.value = false
        modelScreen?.textVisibility?.value = true
        modelScreen?.buttonText?.value = "Reset"
        modelScreen?.textViewMessage?.value = "Success"
    }

    override fun onDataFetchFail(reason: String?) {
        modelScreen?.buttonVisibility?.value = true
        modelScreen?.progressBarVisibility?.value = false
        modelScreen?.textVisibility?.value = true
        modelScreen?.buttonText?.value = "Reset"
        modelScreen?.textViewMessage?.value = reason
    }

    fun setViewModel(screenViewModel: ScreenViewModel?) {
        this.modelScreen = screenViewModel
    }


}