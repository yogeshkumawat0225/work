package com.architecture.myapplication.interactor

import android.content.Context
import android.net.ConnectivityManager
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.architecture.myapplication.view.OnDataFetched

//Interacter
class Interacter(private val onDataFetched: OnDataFetched) {
    private var mRequestQueue: RequestQueue? = null

    fun fetchData(context: Context) {
        if (hasInternetAccess(context)) {
            val url = "https://www.google.com"
            val stringRequest = StringRequest(
                Request.Method.GET, url,
                Response.Listener<String> {
                    onDataFetched.onDataFetchSuccess("Success")
                },
                Response.ErrorListener {
                    onDataFetched.onDataFetchFail(it.message)
                })

            getRequestQueue(context)?.add(stringRequest)
        }
        else {
            onDataFetched.onDataFetchFail("No Internet Connection")
        }
    }

    private fun getRequestQueue(context: Context) : RequestQueue? {
        if(mRequestQueue == null)
            mRequestQueue = Volley.newRequestQueue(context)
        return mRequestQueue
    }

    private fun hasInternetAccess(context: Context): Boolean {
        val mCM = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val netInfo = mCM?.activeNetworkInfo
        return netInfo != null && netInfo.isConnectedOrConnecting
    }

}