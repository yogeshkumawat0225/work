package com.architecture.myapplication.controller

import android.content.Context
import com.architecture.myapplication.entity.ScreenViewModel
import com.architecture.myapplication.interactor.Interacter
import com.architecture.myapplication.presenter.Presenter
import com.architecture.myapplication.view.OnDataFetched

class ViewController : OnDataFetched {

    private val interacter: Interacter = Interacter(this)
    private val presenter: Presenter = Presenter()

    fun postButtonClickEvent(context: Context) {
        interacter.fetchData(context)
    }

    override fun onDataFetchSuccess(response: String) {
        presenter.onDataFetchSuccess(response)
    }

    override fun onDataFetchFail(reason: String?) {
        presenter.onDataFetchFail(reason)
    }

    fun setViewModel(screenViewModel: ScreenViewModel?) {
        presenter?.setViewModel(screenViewModel)
    }
}