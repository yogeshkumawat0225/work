package com.architecture.myapplication.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.architecture.myapplication.R
import com.architecture.myapplication.controller.ViewController
import com.architecture.myapplication.databinding.ActivityMainBinding
import com.architecture.myapplication.entity.ScreenViewModel
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    private val controller: ViewController? = ViewController()
    private lateinit var screenViewModel: ScreenViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding: ActivityMainBinding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        screenViewModel = ViewModelProviders.of(this).get(ScreenViewModel::class.java)
        binding.lifecycleOwner = this
        binding.data = screenViewModel
        controller?.setViewModel(screenViewModel)
        initView()
    }

    private fun initView() {
        button_fetch?.setOnClickListener{
            if(button_fetch?.text == "Reset") {
                screenViewModel?.buttonText?.value = "Fetch"
                screenViewModel?.textVisibility?.value = false
            }
            else {
                screenViewModel?.buttonVisibility?.value = false
                screenViewModel?.progressBarVisibility?.value = true
                controller?.postButtonClickEvent(this)
            }
        }
    }
}
