package com.architecture.myapplication.view

interface OnDataFetched {
    fun onDataFetchSuccess(response: String)
    fun onDataFetchFail(reason: String?)
}